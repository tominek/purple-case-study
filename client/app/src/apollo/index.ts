import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'

const API_URL = process.env.API_URL as string

const httpLink = createHttpLink({
  uri: `${API_URL}/graphql`,
})

const cache = new InMemoryCache()

export default new ApolloClient({
  link: httpLink,
  cache,
})
