const path = require('path')
const express = require('express')

const app = express()
const port = process.env.PORT || 80

app.use(express.static('dist/spa'))

app.get('/health', (req, res) => {
  res.sendStatus(200)
})
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'dist/spa/index.html'))
})

app.listen(port, () => {
  console.log(`Listening on: http://localhost:${port}`)
})
