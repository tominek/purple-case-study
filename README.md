# Currency converter

### What is this repository for?

This is a case study of currency converter with basic UI in Vue3 
and API in Apollo GraphQL and ExpressJs.

- **Client** - UI Client for converting currencies
- **API** - GraphQL API

### How do I get set up?

1. Make a copy of `docker-compose.yml.dist` and rename it to `docker-compose.yml`
2. Add `127.0.0.1 converter.local` to your hosts file
4. Run `docker compose up -d` to run whole stack in docker

### How do I develop locally?

#### API
- API is running in docker in development mode with hot reload.
- In case there is change in `package.json` you will need to rebuild API image
  using `docker compose build api` followed by `docker compose up -d`
  to apply new image build.

#### Client
- Running in docker in production mode.
- Develop it locally using `npm run dev` command, which does hot reload.
- To propagate changes you will need to run `docker compose build client` 
  followed by `docker compose up -d` to apply new image build.

### How do I access applications locally?

- [API / GraphQL playground](http://converter.local/graphql)
- [Client web](http://converter.local)

### Which technologies are used?

- [Apollo GraphQL](https://www.apollographql.com/) (API)
- [ExpressJs](https://expressjs.com/) (API)
- [MongoDB](https://www.mongodb.com/) (API)
- [Vue 3](https://v3.vuejs.org/) (Client)
- [Quasar](https://quasar.dev/) (Client)
- [Docker](https://www.docker.com/)
