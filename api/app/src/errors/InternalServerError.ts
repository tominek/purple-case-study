import { ApolloError } from 'apollo-server'

export default class InternalServerError extends ApolloError {
  constructor(message: string) {
    super(message, 'INTERNAL_SERVER_ERROR')
  }
}
