import { ApolloError } from 'apollo-server'

export default class UnsupportedCurrencyError extends ApolloError {
  constructor(message: string) {
    super(message, 'UNSUPPORTED_CURRENCY_ERROR')
  }
}
