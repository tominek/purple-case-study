import { makeExecutableSchema } from 'graphql-tools'
import { mergeTypes } from 'merge-graphql-schemas'
import { DocumentNode } from 'graphql'

import logger from '../logger'
import loadResolvers from './resolvers'

import General from './general/general.schema'
import Conversion from './conversion/conversion.schema'

const types: DocumentNode[] = [
  General,
  Conversion,
]

const schema = makeExecutableSchema({
  typeDefs: mergeTypes(types, { all: true }),
  resolvers: loadResolvers(),
  logger: {
    log: (e) => logger.error(e.message, e)
  },
  allowUndefinedInResolve: false,
  inheritResolversFromInterfaces: true,
  resolverValidationOptions: {
    requireResolversToMatchSchema: 'warn',
    requireResolversForResolveType: 'warn'
  },
})

export default schema
