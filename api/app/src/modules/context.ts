import { Logger } from 'winston'

import logger from '../logger'
import { getDataLoaders, IDataLoadersContainer } from '../factory/dataLoadersFactory'
import { getDataSources, IDataSourcesContainer } from '../factory/dataSourcesFactory'

export interface IContextContainer extends IDataLoadersContainer, IDataSourcesContainer {
  logger: Logger
}

export default async function createContext (): Promise<IContextContainer> {
  const dataLoaders: IDataLoadersContainer = await getDataLoaders()
  const dataSources: IDataSourcesContainer = getDataSources()

  return {
    logger,
    ...dataLoaders,
    ...dataSources,
  }
}
