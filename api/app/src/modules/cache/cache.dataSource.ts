import Redis from 'ioredis'
import { addHours, endOfDay } from 'date-fns'

import { IExchangeResponse } from '../exchangeRates/exchangeRates.dataSource'

const CACHE_URL = process.env.CACHE_URL as string

export class CacheDataSource {
  private readonly client: Redis.Redis

  constructor () {
    this.client = new Redis(CACHE_URL)
  }

  async set (key: string, value: any, expirationTime: number | undefined = undefined): Promise<void> {
    await this.client.set(key, JSON.stringify(value), 'EX', expirationTime)
  }

  async get<T> (key: string): Promise<T | null> {
    const value = await this.client.get(key)
    if (value) {
      return JSON.parse(value)
    }

    return null
  }

  async setSupportedCurrencies (input: Array<string>): Promise<void> {
    // List of currencies is renewed every day
    const expiration = Math.round((endOfDay(new Date()).getTime() - Date.now()) / 1000)
    await this.set('supported-currencies', input, expiration)
  }

  async getSupportedCurrencies (): Promise<Array<string> | null> {
    return await this.get<Array<string>>('supported-currencies')
  }

  async setConversionRate (base: string, input: IExchangeResponse): Promise<void> {
    // Conversion rates are updated every hour
    const expiration = Math.round((addHours(new Date(), 1).getTime() - Date.now()) / 1000)
    await this.set(`rates-${base}`, input, expiration)
  }

  async getConversionRates (base: string): Promise<IExchangeResponse | null> {
    return await this.get<IExchangeResponse>(`rates-${base}`)
  }
}
