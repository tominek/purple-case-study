import axios, { AxiosInstance } from 'axios'

const EXCHANGE_APP_ID = process.env.EXCHANGE_APP_ID as string
const EXCHANGE_RATE_URL = 'https://openexchangerates.org/api/'

export interface IExchangeResponse {
  timestamp: number
  base: string
  rates: Record<string, number>
}

export class ExchangeRatesDataSource {
  private readonly client: AxiosInstance

  constructor () {
    this.client = axios.create({
      baseURL: EXCHANGE_RATE_URL,
    })
  }

  async getCurrencies (): Promise<Array<string>> {
    const result = await this.client.get('/currencies.json')
    return result?.data
      ? Object.entries(result.data).map(([key]) => key)
      : []
  }

  async getConversionRates (base: string): Promise<IExchangeResponse> {
    const result = await this.client.get('/latest.json', {
      params: {
        app_id: EXCHANGE_APP_ID,
        base,
      },
    })

    return result.data
  }
}
