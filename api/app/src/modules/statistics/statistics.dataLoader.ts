import SuperDataLoader, { createLoader, getCollection, IDocument } from '../../helpers/dataLoader'

interface IStatistic extends IDocument {
  type: string
  value: number
}

export default class StatisticsDataLoader extends SuperDataLoader<IStatistic> {
  static async getInstance (): Promise<StatisticsDataLoader> {
    const collection = await getCollection('statistics')
    return new StatisticsDataLoader(collection, await createLoader(collection))
  }

  async incrementConvertedAmount (amount: number): Promise<void> {
    const convertedAmount = (await this.getCollection().findOneAndUpdate(
      { type: 'convertedAmount' },
      { $inc: { value: amount } },
    )).value

    if (!convertedAmount) {
      await this.getCollection().insertOne({
        type: 'convertedAmount',
        value: amount,
      })
    }
  }

  async getConvertedAmount (): Promise<number> {
    const counter = await this.getCollection().findOne(
      { type: 'convertedAmount' },
    )
    return counter?.value ?? 0
  }
}
