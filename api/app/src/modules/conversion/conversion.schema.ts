import { gql } from 'apollo-server'
import { DocumentNode } from 'graphql'

const types: DocumentNode = gql`
    input ConversionInput {
        amount: Float!
        baseCurrency: Currency!
        targetCurrency: Currency!
    }

    type Mutation {
        convert(input: ConversionInput!): ConversionResult!
    }

    type Query {
        supportedCurrencies: [Currency]!
        mostPopularCurrency: Currency
        totalConvertedInUSD: Float!
        conversionsCount: BigInt!
    }

    type ConversionResult {
        amount: Float!
        baseCurrency: Currency!
        targetCurrency: Currency!
        convertedAmount: Float!
    }
`

export default types
