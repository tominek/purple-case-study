import { IDocument } from '../../helpers/dataLoader'

export interface IConversion extends IDocument{
  amount: number
  baseCurrency: string
  targetCurrency: string
  convertedAmount: number
}
