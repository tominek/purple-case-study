import { IModuleResolvers } from '../resolvers'
import { IConversion } from './conversion.interfaces'
import UnsupportedCurrencyError from '../../errors/UnsupportedCurrencyError'
import { getConversionRates, incrementConvertedAmount } from './conversion.helpers'

const resolvers: IModuleResolvers = {
  queries: {
    supportedCurrencies: async (parent, args, { Cache, ExchangeRates }): Promise<Array<string>> => {
      let currencies = await Cache.getSupportedCurrencies()
      if (!currencies) {
        currencies = await ExchangeRates.getCurrencies()
        await Cache.setSupportedCurrencies(currencies)
      }

      return currencies
    },
    mostPopularCurrency: async (parent, args, { Conversion }): Promise<string | null> => {
      return await Conversion.getMostPopularCurrency()
    },
    totalConvertedInUSD: async (parent, args, { Statistics }): Promise<number> => {
      return await Statistics.getConvertedAmount()
    },
    conversionsCount: async (parent, args, { Conversion }): Promise<number> => {
      return await Conversion.count({})
    },
  },
  mutations: {
    convert: async (
      parent,
      { input: { amount, baseCurrency, targetCurrency } },
      context,
    ): Promise<IConversion> => {
      const { Conversion } = context

      const conversionRates = await getConversionRates(baseCurrency, context)
      const rate = conversionRates.rates?.[targetCurrency]
      if (rate === undefined) {
        throw new UnsupportedCurrencyError(`Target currency ${targetCurrency as string} is not supported.`)
      }

      await incrementConvertedAmount(baseCurrency, amount, 'USD', context)

      return await Conversion.create({
        amount, baseCurrency, targetCurrency,
        convertedAmount: amount * rate,
      })
    },
  },
  resolvers: {},
}

export default resolvers
