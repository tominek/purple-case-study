import { Request, Response } from 'express'

import logger from './logger'

const requestLogger = (req: Request, res: Response, next: () => unknown) => {
  logger.debug(`${req.method} ${req.url}`, { body: req.body })
  next()
}

export default requestLogger
