import { GraphQLScalarType } from 'graphql'

export default new GraphQLScalarType({
  name: 'Currency',
  description: 'Currency string',
  serialize: (value) => {
    return value
  },
  parseValue: (value) => {
    return value
  },
  parseLiteral: (ast: any) => {
    return ast.value
  }
})
