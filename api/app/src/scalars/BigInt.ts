import { GraphQLScalarType } from 'graphql'

export default new GraphQLScalarType({
  name: 'BigInt',
  description: 'Big integer for handling bigger numbers',
  serialize: (value) => {
    return value
  },
  parseValue: (value) => {
    return value
  },
  parseLiteral: (ast: any) => {
    return ast.value
  }
})
