import { createLogger, format, Logger, transports } from 'winston'
const { colorize, combine, json, timestamp, metadata, printf } = format

const simpleFormat = combine(
  metadata(),
  colorize(),
  timestamp(),
  printf((info) => {
    return `${info.timestamp} [${info.level}] ${info.message} ${JSON.stringify(info.metadata)}` // eslint-disable-line
  })
)

const prodFormat = combine(timestamp(), json())

const logger: Logger = createLogger({
  level: 'silly',
  format: process.env.NODE_ENV === 'development' ? simpleFormat : prodFormat,
  transports: [new transports.Console()],
  handleExceptions: true,
  exitOnError: false
})

export default logger
