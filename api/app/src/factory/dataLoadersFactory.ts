import ConversionDataLoader from '../modules/conversion/conversion.dataLoader'
import StatisticsDataLoader from '../modules/statistics/statistics.dataLoader'

export async function getDataLoaders(): Promise<IDataLoadersContainer> {
  return {
    Conversion: await ConversionDataLoader.getInstance(),
    Statistics: await StatisticsDataLoader.getInstance(),
  }
}

export interface IDataLoadersContainer {
  Conversion: ConversionDataLoader
  Statistics: StatisticsDataLoader
}
