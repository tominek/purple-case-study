import cors from 'cors'
import { hidePoweredBy } from 'helmet/dist'
import express from 'express'
import * as Sentry from '@sentry/node'
import bodyParser from 'body-parser'

import apolloServer from './src/server'
import logger from './src/logger'
import { inProduction, notInProduction } from './src/helpers/general'
import { closeConnection } from './src/helpers/dataLoader'
import requestLogger from './src/requestLogger'

const port = process?.env?.PORT ?? 80
const app = express()

if (inProduction()) {
  Sentry.init({
    enabled: process?.env?.SENTRY_ENABLED === 'true',
    dsn: process?.env?.SENTRY_DSN,
    tracesSampleRate: 1.0,
    debug: notInProduction(),
    environment: process?.env?.APP_ENV ?? 'unknown',
  })
}

app.use(bodyParser.json())
app.use(cors())
app.use(hidePoweredBy())

if (inProduction()) {
  app.use(Sentry.Handlers.requestHandler())
}

if (notInProduction()) {
  app.use(requestLogger)
}

app.get('/health', (req, res) => {
  res.sendStatus(200)
})

apolloServer.applyMiddleware({ app })

if (inProduction()) {
  app.use(Sentry.Handlers.errorHandler())
}

const server = app.listen(port, () => {
  logger.info(`🚀 Server ready at http://localhost:${port}/graphql`)
})

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)

async function shutdown (): Promise<void> {
  logger.info('Received kill signal, shutting down gracefully')
  await closeConnection()
  server.close(() => {
    logger.info('Closed out remaining connections')
    process.exit(0)
  })
  setTimeout(() => {
    logger.error('Could not close connections in time, forcefully shutting down')
    process.exit(1)
  }, 10000)
}
