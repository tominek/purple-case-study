ARG NODE_IMAGE=node:lts-alpine

FROM $NODE_IMAGE as dev
ENV DOCKER_STAGE=dev
COPY .docker/entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh

WORKDIR /app
COPY ./app .
RUN npm install
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
EXPOSE 80
